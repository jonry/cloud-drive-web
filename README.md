# Cloud Drive Web

Frontend application for Cloud Drive (see https://gitlab.com/jonry/cloud-drive)

## Features
- User Management
- File Browsing, Download and Upload
- (more to come)


## Support
For support open issues in this repo.

## Roadmap
- Use webinterface as full featured file browser

## Contributing
If you want to contribute, create a merge request.

## Authors and acknowledgment
- https://gitlab.com/jonry
- https://gitlab.com/davidschnrr

## License
MIT License

## Project status
Status: Active

Working on it during free time.